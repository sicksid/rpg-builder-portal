#!/usr/bin/env python3
if __name__ == "__main__":
    from app import create_app
    flask_app = create_app()
    flask_app.config['DEBUG'] = True
    flask_app.run("0.0.0.0")
