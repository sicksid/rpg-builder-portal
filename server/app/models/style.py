from orator.orm import has_many, belongs_to
from app import db, factory


class Style(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'name'
    ]

    @belongs_to
    def project(self):
        from .project import Project
        return Project

    @has_many
    def elevations(self):
        from .elevation import Elevation
        return Elevation


@factory.define(Style)
def styles_factory(faker):
    return {
        "name": faker.word()
    }
