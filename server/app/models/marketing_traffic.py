from orator.orm import has_one, belongs_to
from dateutil.parser import parse
from app import db, factory


class MarketingTraffic(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'weekday_visitors',
        'weekend_visitors',
        'start_on',
        'end_on'
    ]

    @belongs_to
    def project(self):
        from .project import Project
        return Project

    @belongs_to
    def builder(self):
        from .builder import Builder
        return Builder

    @belongs_to
    def created_by(self):
        from .user import User
        return User

    def get_dates(self):
        return ['created_at', 'updated_at', 'start_on', 'end_on']

@factory.define(MarketingTraffic)
def marketing_traffics_factory(faker):
    start_on = parse(faker.date()).date()
    end_on = faker.future_date(end_date="+6d")
    return {
        "weekday_visitors": faker.pyint(),
        "weekend_visitors": faker.pyint(),
        "start_on": start_on,
        "end_on": end_on,
    }
