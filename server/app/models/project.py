from orator.orm import has_many
from app import db, factory


class Project(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'name',
        'description',
        'config'
    ]
    __casts__ = {
        'config': 'dict'
    }

    @has_many
    def lots(self):
        from .lots import Lot
        return Lot


@factory.define(Project)
def projects_factory(faker):
    return {
        "name": faker.company(),
        "description": faker.text()
    }
