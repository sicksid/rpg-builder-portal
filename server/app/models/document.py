from orator.orm import has_one, has_many, belongs_to
from app import db, factory


class Document(db.Model):

    @belongs_to
    def created_by(self):
        from .user import User
        return User

    @belongs_to
    def plan(self):
        from .plan import Plan
        return Plan

    @belongs_to
    def elevation(self):
        from .elevation import Elevation
        return Elevation

    @belongs_to
    def spec(self):
        from .spec import Spec
        return Spec

    @has_many
    def builders(self):
        from .builder import Builder
        return Builder

    @belongs_to
    def project(self):
        from .project import Project
        return Project
