from orator.orm import belongs_to
from random import choice
from app import db, factory


class Material(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'name',
        'category',
        'manufacturer',
        'status',
        'type',
    ]

    @belongs_to
    def uploaded_by(self):
        from .user import User
        return User


@factory.define(Material)
def materials_factory(faker):
    return {
        "name": faker.color_name(),
        "category": faker.word(),
        "manufacturer": faker.company(),
        "status": 'pending',
        "type": choice(['roofing', 'siding', 'secondary'])
    }


@factory.define(Material, 'approved')
def approved_materials_factory(faker):
    material = factory.raw(Material)
    material.update({
        'status': 'approved'
    })
    return material


@factory.define(Material, 'denied')
def denied_materials_factory(faker):
    material = factory.raw(Material)
    material.update({
        'status': 'denied'
    })
    return material
