from random import choice
from app import db, factory


class Rule(db.Model):
    __guarded__ = [
        'id'
    ]

    __fillable__ = [
        'side',
        'operation',
        'quantity',
    ]


@factory.define(Rule)
def ruless_factory(faker):
    return {
        'side': choice(['left', 'right', 'front', 'back']),
        'operation': choice([
            'more than',
            'more or equal than',
            'less than',
            'less or equal than',
            'equals',
            'percentage'
        ]),
        'quantity': faker.pyint()
    }
