from orator.orm import belongs_to
from app import db, factory


class Realtor(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'name',
        'email',
        'phone',
        'agency',
        'agency_phone',
        'agency_website',
        'agency_address',
    ]

    @belongs_to
    def created_by(self):
        from .user import User
        return User


@factory.define(Realtor)
def realtors_factory(faker):
    return {
        "name": faker.name(),
        "email": faker.email(),
        "phone": faker.phone_number(),
        "agency": faker.company(),
        "agency_phone": faker.phone_number(),
        "agency_website": faker.url(),
        "agency_address": faker.street_address(),
    }
