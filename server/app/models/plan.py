from orator.orm import belongs_to
from random import choice, randint
from app import db, factory


class Plan(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'name',
        'description',
        'stories',
        'size',
        'min_beds',
        'max_beds',
        'min_baths',
        'max_baths',
        'min_garage',
        'max_garage',
        'min_sqft',
        'max_sqft',
        'min_price',
        'max_price',
        'front_porch',
        'front_loaded',
    ]

    @belongs_to
    def project(self):
        from .project import Project
        return Project

    @belongs_to
    def builder(self):
        from .builder import Builder
        return Builder

    @belongs_to
    def created_by(self):
        from .user import User
        return User

@factory.define(Plan)
def plans_factory(faker):
    return {
        'name': faker.random_letter(),
        'description': faker.text(),
        'stories': randint(0, 3),
        'size': faker.pyint(),
        'min_beds': randint(0, 3),
        'max_beds': randint(3, 6),
        'min_baths': randint(0, 3),
        'max_baths': randint(3, 6),
        'min_garage': randint(0, 1),
        'max_garage': randint(1, 2),
        'min_sqft': faker.pyint(),
        'max_sqft': faker.pyint(),
        'min_price': faker.pyint(),
        'max_price': faker.pyint(),
        'front_porch': faker.boolean(),
        'front_loaded': faker.boolean(),
    }
