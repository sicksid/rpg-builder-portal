from orator.orm import belongs_to, belongs_to_many
from app import db, factory


class Builder(db.Model):
    __guarded__ = ['id']

    __fillable__ = [
        'schedule',
        'name',
        'website',
        'description',
        'address',
        'second_address',
        'city',
        'phone',
        'state',
        'zipcode',
        'fax',
        'approved_at',
    ]

    __casts__ = {
        'schedule': 'dict',
    }

    @belongs_to
    def approved_by(self):
        from .user import User
        return User

    @belongs_to_many
    def materials(self):
        from .material import Material
        return Material

@factory.define(Builder)
def builders_factory(faker):
    return {
        "name": faker.company(),
        "website": faker.url(),
        "description": faker.text(),
        "address": faker.street_address(),
        "second_address": faker.street_address(),
        "city": faker.city(),
        "state": faker.state(),
        "zipcode": faker.zipcode(),
        "phone": faker.phone_number(),
        "fax": faker.phone_number(),
    }
