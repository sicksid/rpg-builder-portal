from orator.orm import belongs_to, scope
from datetime import datetime
from app import db, factory


class Sale(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'price',
        'sqft',
        'project_id',
        'buyer_id',
        'realtor_id',
        'is_closed',
        'is_cancelled',
        'sold_on',
        'cancelled_on',
        'estimated_close_on',
    ]

    __casts__ = {
        'is_cancelled': 'bool',
        'is_closed': 'bool',
    }

    def get_dates(self):
        return [
            'sold_on',
            'closed_on',
            'cancelled_on',
            'estimated_close_on',
            'created_at',
            'updated_at',
        ]

    @scope
    def cancellations(self, query):
        return query.where_is_cancelled(True)

    @scope
    def closings(self, query):
        return query.where_is_closed(True)

    def cancel(self):
        self.clear()
        self.is_cancelled = True
        self.cancelled_on = datetime.now().date()
        self.save()

    def close(self):
        self.clear()
        self.is_closed = True
        self.closed_on = datetime.now().date()
        self.save()

    def clear(self):
        self.is_closed = False
        self.is_cancelled = False
        self.closed_on = None
        self.cancelled_on = None
        self.save()

    @belongs_to
    def lot(self):
        from .lot import Lot
        return Lot

    @belongs_to
    def buyer(self):
        from .buyer import Buyer
        return Buyer

    @belongs_to
    def realtor(self):
        from .realtor import Realtor
        return Realtor

    @belongs_to
    def created_by(self):
        from .user import User
        return User


@factory.define(Sale)
def sales_factory(faker):
    return {
        "price": faker.pyint(),
        "sqft": faker.pyint(),
        "is_closed": False,
        "is_cancelled": False,
        "estimated_close_on": faker.future_date(end_date="+15d")
    }


@factory.define(Sale, 'closing')
def closings_factory(faker):
    sale = factory.raw(Sale)
    sale.update({
        'is_closed': True,
        'closed_on': faker.date_between_dates(date_end=sale['estimated_close_on']),
        'sold_on': faker.date_between_dates(date_end=sale['estimated_close_on']),
    })
    return sale


@factory.define(Sale, 'cancellation')
def cancellations_factory(faker):
    sale = factory.raw(Sale)
    sale.update({
        'is_cancelled': True,
        'cancelled_on': faker.future_date(end_date="+30d")
    })
    return sale
