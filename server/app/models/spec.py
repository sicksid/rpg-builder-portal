from orator.orm import belongs_to, scope
from random import choice
from app import db, factory


class Spec(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'price',
        'sqft',
        'front_porch',
        'description',
        'is_arc',
        'available_on',
    ]

    def get_dates(self):
        return [
            'available_on',
            'created_at',
            'updated_at',
            'denied_on',
            'approved_on',
        ]

    @scope
    def arcs(self, query):
        return query.where_is_arc(True)

    @belongs_to
    def lot(self):
        from .lot import Lot
        return Lot

    @belongs_to
    def project(self):
        from .project import Project
        return Project

    @belongs_to
    def created_by(self):
        from .user import User
        return User


@factory.define(Spec)
def specs_factory(faker):
    return {
        "price": faker.pyint(),
        "sqft": faker.pyint(),
        "front_porch": faker.boolean(),
        "description": faker.text(),
        "is_arc": False,
        "available_on": faker.date_object(),
    }


@factory.define(Spec, 'arc')
def arcs_factory(faker):
    return {
        "price": faker.pyint(),
        "sqft": faker.pyint(),
        "front_porch": faker.boolean(),
        "description": faker.text(),
        "is_arc": True,
        "available_on": faker.date_object(),
    }
