from orator.orm import has_one, has_many, belongs_to
from random import choice
from app import db, factory


class Elevation(db.Model):
    __guarded__ = ['id']
    __fillable = [
        'name',
        'description',
        'front_porch',
    ]


    def get_dates(self):
        return [
            'denied_on',
            'approved_on',
            'created_at',
            'updated_at'
        ]

    @belongs_to
    def plan(self):
        from .plan import Plan
        return Plan

    @belongs_to
    def style(self):
        from .style import Style
        return Style

    @belongs_to
    def created_by(self):
        from .user import User
        return User

    @belongs_to
    def project(self):
        from .project import Project
        return Project

@factory.define(Elevation)
def elevations_factory(faker):
    return {
        "status": 'pending',
        "name": faker.word(),
        "description": faker.text(),
        "front_porch": faker.boolean(),
    }


@factory.define(Elevation, 'approved')
def approved_elevations_factory(faker):
    elevation = factory.raw(Elevation)
    elevation.update({
        "status": "approved",
        "approved_on": faker.date_object()
    })


@factory.define(Elevation, 'denied')
def denied_elevations_factory(faker):
    elevation = factory.raw(Elevation)
    elevation.update({
        "status": "denied",
        "denied_on": faker.date_object()
    })
