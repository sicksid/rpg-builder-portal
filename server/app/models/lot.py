from orator.orm import has_one, belongs_to
from random import choice, randint
from app import db, factory

class Lot(db.Model):
    __guarded__ = [
        'id'
    ]

    __fillable__ = [
        'address',
        'size',
        'sku',
        'description',
        'street',
        'street_block',
        'block',
        'status',
        'type',
        'custom',
        'length',
        'polygon'
    ]

    __casts__ = {
        'polygon': 'list'
    }

    @belongs_to
    def project(self):
        from .project import Project
        return Project

    @belongs_to
    def builder(self):
        from .builder import Builder
        return Builder

    @belongs_to
    def spec(self):
        from .spec import Spec
        return Spec
    
    @belongs_to
    def sale(self):
        from .sale import Sale
        return Sale

@factory.define(Lot)
def lots_factory(faker):
    block = faker.random_letter()
    street_name = faker.street_name()
    street_block = f"{street_name} - {block}"

    return {
        'address': faker.street_address(),
        'size': faker.pyint(),
        'sku': faker.uuid4(),
        'description': faker.text().replace('\n', ' '),
        'street': street_name,
        'street_block': street_block,
        'block': block,
        'status': choice([
            'available',
            'sold',
            'spec',
            'reserved',
            'closed',
            'arc',
            'coming soon'
        ]),
        'type': choice([
            'build',
            'move in',
            'construction',
            'window',
            'model',
            'occupied',
            'vacant',
            'trailer',
            'reserved',
            'vacant lot'
        ]),
        'polygon': [[faker.latitude(), faker.longitude()] for index in range(randint(4, 9))],
        'custom': faker.boolean(),
        'length': faker.pyint(),
    }
