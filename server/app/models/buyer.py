from random import randint
from orator.orm import belongs_to, has_one
from app import db, factory


class Buyer(db.Model):
    __guarded__ = ['id']
    __fillable__ = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'spouse_first_name',
        'spouse_last_name',
        'spouse_email',
        'spouse_phone',
        'address',
        'employer_name',
        'employer_phone',
        'employer_zipcode',
        'spouse_employer_name',
        'spouse_employer_phone',
        'spouse_employer_zipcode',
        'children',
        'age',
        'married',
        'number_of_children',
    ]

    __casts__ = {
        'children': 'dict'
    }

    @belongs_to
    def created_by(self):
        from .user import User
        return User

    @belongs_to
    def sale(self):
        from .sale import Sale
        return Sale

@factory.define(Buyer)
def buyers_factory(faker):
    data = {
        "first_name": faker.first_name(),
        "last_name": faker.last_name(),
        "email": faker.email(),
        "phone": faker.phone_number(),
        "spouse_first_name": faker.first_name(),
        "spouse_last_name": faker.last_name(),
        "spouse_email": faker.email(),
        "spouse_phone": faker.phone_number(),
        "address": faker.street_address(),
        "employer_name": faker.name(),
        "employer_phone": faker.phone_number(),
        "employer_zipcode": faker.zipcode(),
        "spouse_employer_name": faker.name(),
        "spouse_employer_phone": faker.phone_number(),
        "spouse_employer_zipcode": faker.zipcode(),
        "age": randint(1, 25),
        "married": faker.boolean(),
        "number_of_children": randint(0, 5),
    }
    if data.get('married'):
        for index in range(1, data['number_of_children']):
            if not data.get('children'):
                data['children'] = []
            data['children'].append({
                'age': randint(0, data['age']),
                'id': index
            })
    return data
