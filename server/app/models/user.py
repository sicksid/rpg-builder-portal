from orator.orm import belongs_to_many, belongs_to
from random import choice
from app import db, factory


class User(db.Model):
    __guarded__ = ['id', 'password']
    __fillable__ = [
        'email',
        'second_email',
        'first_name',
        'last_name',
        'phone',
        'title',
        'role',
    ]
    __casts__ = {
        'schedule': 'dict',
        'settings': 'dict'
    }

    @belongs_to
    def active_project(self):
        from .project import Project
        return Project

    @belongs_to
    def builder(self):
        from .builder import Builder
        return Builder

    @belongs_to_many
    def projects(self):
        from .project import Project
        return Project


@factory.define(User)
def users_factory(faker):
    return {
        "email": faker.email(),
        "second_email": faker.email(),
        "first_name": faker.first_name(),
        "last_name": faker.last_name(),
        "phone": faker.phone_number(),
        "title": faker.job(),
        "recovery_token": None,
        "role": choice([
            'admin',
            'consultant',
            'comunity manager',
            'investor',
            'executive',
            'partner',
            'staff',
            'super user',
        ])
    }


@factory.define(User, 'admin')
def admins_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'admin'
    })
    return user


@factory.define(User, 'consultant')
def consultants_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'consultant'
    })
    return user


@factory.define(User, 'comunity manager')
def comunity_managers_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'comunity manager'
    })
    return user


@factory.define(User, 'investor')
def investors_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'investor'
    })
    return user


@factory.define(User, 'executive')
def executives_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'executive'
    })
    return user


@factory.define(User, 'staff')
def staffs_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'staff'
    })
    return user


@factory.define(User, 'super user')
def super_users_factory(faker):
    user = factory.raw(User)
    user.update({
        'role': 'super user'
    })
    return user
