from marshmallow import fields, Schema

class LotSchema(Schema):
    address = fields.Str()
    size = fields.Decimal()
    sku = fields.UUID()
    description = fields.Str()
    street = fields.Str()
    street_block = fields.Str()
    block = fields.Str()
    status = fields.Str()
    type = fields.Str()
    custom = fields.Boolean()
    length = fields.Decimal()
    polygon = fields.List(fields.List(fields.Decimal()))
    spec = fields.Nested('SpecSchema')
    sale = fields.Nested('SaleSchema')
    project = fields.Nested('ProjectSchema')