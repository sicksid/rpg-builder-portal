from marshmallow import fields, Schema

default = {
    'monday': None,
    'tuesday': None,
    'wednesday': None,
    'thursday': None,
    'friday': None,
    'saturday': None,
    'sunday': None
}

class BuilderSchema(Schema):
    id = fields.Int()
    schedule = fields.Dict(default=default)
    name = fields.Str()
    website = fields.Str()
    description = fields.Str()
    address = fields.Str()
    second_address = fields.Str()
    city = fields.Str()
    state = fields.Str()
    zipcode = fields.Str()
    phone = fields.Str()
    fax = fields.Str()
    approved_at = fields.DateTime()
    custom = fields.Boolean()
    length = fields.Decimal()
