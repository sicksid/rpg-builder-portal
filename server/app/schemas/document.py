from marshmallow import fields, Schema


class DocumentSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    tags = fields.Str()
    project = fields.Nested('ProjectSchema')
    plan = fields.Nested('PlanSchema')
    elevation = fields.Nested('ElevationSchema')
    spec = fields.Nested('SpecSchema')
