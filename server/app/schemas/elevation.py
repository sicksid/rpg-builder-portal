from marshmallow import fields, Schema


class ElevationSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    front_porch = fields.Bool()
    plan = fields.Nested('PlanSchema')
    status = fields.Str()
    style = fields.Nested('StyleSchema')
    project = fields.Nested('ProjectSchema')
    available_on = fields.Date()
    denied_on = fields.Date()