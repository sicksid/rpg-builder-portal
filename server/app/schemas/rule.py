from marshmallow import fields, Schema


class RuleSchema(Schema):
    side = fields.Str()
    quantity = fields.Int()
    operation = fields.Str()
