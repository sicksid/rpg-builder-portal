from marshmallow import fields, Schema


class StyleSchema(Schema):
    name = fields.Str()
    project = fields.Nested('ProjectSchema')
