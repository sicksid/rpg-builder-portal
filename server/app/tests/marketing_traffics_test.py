from datetime import datetime, timedelta
from app.tests import TestCaseMixin
from app.models import MarketingTraffic


class TestMarketingTrafficResource(TestCaseMixin):
    def setUp(self):
        self.marketing_traffic = self.factory(MarketingTraffic).create()

    def tearDown(self):
        MarketingTraffic.destroy(MarketingTraffic.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(
            f"/api/reports/marketing/{self.marketing_traffic.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'weekday_visitors': 3,
            'weekend_visitors': 12,
            'start_on': str(datetime.now().date()),
            'end_on': str((datetime.now() + timedelta(days=6)).date()),
        }
        response = self.client.put(
            f"/api/reports/marketing/{self.marketing_traffic.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        marketing_traffic = MarketingTraffic.find(self.marketing_traffic.id)
        self.assertEqual(marketing_traffic.weekday_visitors, data['weekday_visitors'])
        self.assertEqual(marketing_traffic.weekend_visitors, data['weekend_visitors'])
        self.assertEqual(response.status_code, 200)


class TestMarketingTrafficsResource(TestCaseMixin):
    def setUp(self):
        pass

    def tearDown(self):
        MarketingTraffic.destroy(*MarketingTraffic.all().pluck('id').all())

    def test_get(self):
        self.factory(MarketingTraffic, 200).create()
        response = self.client.get('/api/reports/marketing')
        self.assertEquals(len(response.get_json()), 200)

    def test_post(self):
        data = self.factory(MarketingTraffic).make().serialize()
        response = self.client.post(
            '/api/reports/marketing',
            data=self.json.dumps(data, cls=self.app.config['RESTFUL_JSON']['cls']),
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()

        for key, value in data.items():
            self.assertEqual(str(response_data[key]), str(value))

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
