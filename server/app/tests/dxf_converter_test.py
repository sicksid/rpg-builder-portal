from unittest import TestCase, skip
from os.path import exists as file_exists
from os import remove as file_remove
from os.path import join as path_join
from app.lib import DxfConverter


class TestDxfConverter(TestCase):
    def setUp(self):
        #self.converter = DxfConverter('/home/deployer/shared/files/lots.dxf')
        pass

    @skip('not implemented yet')
    def test_initialize(self):
        self.assertIsNotNone(self.converter.input)
        self.assertIn('/home/deployer/shared/files/lots.dxf', self.converter.input)

    @skip('not implemented yet')
    def test_store(self):
        path = '/tmp'
        self.converter.store(path)
        self.assertTrue(file_exists(path))
        self.assertIsNone(self.converter.output)
        file_remove(path_join(path, 'lots.shp'))
