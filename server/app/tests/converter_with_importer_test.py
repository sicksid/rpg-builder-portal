import json
from unittest import TestCase, skip
from os.path import exists as file_exists
from os import remove as file_remove
from os.path import join as path_join
from app.lib import DxfConverter, ShapefileImporter


class TestDxfConverter(TestCase):

    @skip('not implemented yet')
    def test_convert_importer(self):
        path = '/tmp'
        converter = DxfConverter('/home/deployer/shared/files/lots.dxf')
        converter.store(path)
        self.assertIsNone(converter.shp)
        self.assertIsNone(converter.shx)
        self.assertIsNone(converter.dbf)
        importer = ShapefileImporter('/tmp/lots.shp')
        self.assertTrue(file_exists(path + '/lots.shp'))
        self.assertTrue(file_exists(path + '/lots.shx'))
        self.assertTrue(file_exists(path + '/lots.dbf'))
        with open('/tmp/lots.geojson', 'w+') as f:
            f.write(json.dumps(importer.data))
        file_remove(path_join(path, 'lots.shp'))
        file_remove(path_join(path, 'lots.shx'))
        file_remove(path_join(path, 'lots.dbf'))
