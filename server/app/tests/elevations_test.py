from app.tests import TestCaseMixin
from app.models import Elevation, Plan, Style


# class ElevationTestCase(TestCaseMixin):
#     def setUp(self):
#         pass

#     def test_add_materials(self):
#         from ..models import Elevation, Material
#         elevation = self.factory(Elevation).create()
#         material = self.factory(Material).create()

#         elevation.materials().save(material)
#         self.assertEquals(elevation.materials().get_results().count(), 1)


class TestElevationResource(TestCaseMixin):
    def setUp(self):
        self.plan = self.factory(Plan).create()
        self.elevation = self.factory(Elevation).make()
        self.elevation.plan_id = self.plan.id
        self.elevation.save()

    def tearDown(self):
        Elevation.destroy(Elevation.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/elevations/{self.elevation.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'description': 'TEST Description',
            'name': 'Walsh'
        }
        response = self.client.put(
            f"/api/elevations/{self.elevation.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        elevation = Elevation.find(self.elevation.id)
        self.assertEqual(elevation.name, data['name'])
        self.assertEqual(elevation.description, data['description'])
        self.assertEqual(response.status_code, 200)


class TestElevationsResource(TestCaseMixin):
    maxDiff = None

    def setUp(self):
        pass

    def tearDown(self):
        Elevation.destroy(*Elevation.all().pluck('id').all())

    def test_get(self):
        self.factory(Elevation, 100).create()
        response = self.client.get('/api/elevations')
        self.assertEquals(len(response.get_json()), 100)

    def test_post(self):
        plan = self.factory(Plan).make().serialize()
        style = self.factory(Style).make().serialize()
        data = self.factory(Elevation).make().serialize()
        data['plan'] = plan
        data['style'] = style
        data = self.json.dumps(data)
        response = self.client.post(
            '/api/elevations',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()

        for key, value in self.json.loads(data).items():
            if key in ['style', 'plan']:
                del response_data[key]['id']
                del response_data[key]['created_at']
                del response_data[key]['updated_at']
                self.assertDictEqual(response_data[key], value)
            else:
                self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
