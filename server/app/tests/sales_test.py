from app.tests import TestCaseMixin
from app.models import Sale, Buyer, Realtor, Project


class TestSaleResource(TestCaseMixin):
    def setUp(self):
        self.sale = self.factory(Sale).create()

    def tearDown(self):
        Sale.destroy(Sale.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/sales/{self.sale.id}")
        self.assertEqual(response.status_code, 200)

    def test_close_sale(self):
        data = {
            'is_closed': True,
        }
        response = self.client.put(
            f"/api/sales/{self.sale.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        sale = Sale.find(self.sale.id)
        self.assertEqual(sale.is_closed, True)
        self.assertIsNotNone(sale.closed_on)
    
    def test_cancel_sale(self):
        data = {
            'is_cancelled': True,
        }
        response = self.client.put(
            f"/api/sales/{self.sale.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        sale = Sale.find(self.sale.id)
        self.assertEqual(sale.is_cancelled, True)
        self.assertIsNotNone(sale.cancelled_on)

    def test_put(self):
        data = {
            'price': 1000000,
            'sqft': 2900
        }
        response = self.client.put(
            f"/api/sales/{self.sale.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        sale = Sale.find(self.sale.id)
        self.assertEqual(sale.price, data['price'])
        self.assertEqual(sale.sqft, data['sqft'])
        self.assertEqual(response.status_code, 200)


class TestSalesResource(TestCaseMixin):
    maxDiff = None

    def tearDown(self):
        Sale.destroy(*Sale.all().pluck('id').all())

    def test_get(self):
        self.factory(Sale, 100).create()
        response = self.client.get('/api/sales')
        self.assertEquals(len(response.get_json()), 100)

    def test_get_cancellations(self):
        self.factory(Sale, 'cancellation', 200).create()
        response = self.client.get("/api/sales", data={'is_cancelled': True})
        self.assertEqual(response.status_code, 200)

    def test_get_closings(self):
        self.factory(Sale, 'closing', 200).create()
        response = self.client.get(
            "/api/sales", data={'is_closed': True})
        self.assertEqual(response.status_code, 200)
    
    def test_post(self):
        sale = self.factory(Sale).make().serialize()
        realtor = self.factory(Realtor).make().serialize()
        buyer = self.factory(Buyer).make().serialize()
        sale['realtor'] = realtor
        sale['buyer'] = buyer
        data = self.json.dumps(sale)
        response = self.client.post(
            '/api/sales',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()
        for key, value in self.json.loads(data).items():
            if key in ['buyer', 'realtor']:
                del response_data[key]['id']
                del response_data[key]['created_at']
                del response_data[key]['updated_at']
                self.assertDictEqual(response_data[key], value)
            else:
                self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
