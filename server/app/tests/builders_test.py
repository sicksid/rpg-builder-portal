from app.tests import TestCaseMixin
from app.models import Builder


class BuilderTestCase(TestCaseMixin):
    def setUp(self):
        pass

    def test_add_materials(self):
        from ..models import Builder, Material
        builder = self.factory(Builder).create()
        material = self.factory(Material).create()

        builder.materials().save(material)
        self.assertEquals(builder.materials().get_results().count(), 1)


class TestBuilderResource(TestCaseMixin):
    def setUp(self):
        self.builder = self.factory(Builder).create()

    def tearDown(self):
        Builder.destroy(Builder.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/builders/{self.builder.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'description': 'TEST Description',
            'name': 'Walsh'
        }
        response = self.client.put(
            f"/api/builders/{self.builder.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        builder = Builder.find(self.builder.id)
        self.assertEqual(builder.name, data['name'])
        self.assertEqual(builder.description, data['description'])
        self.assertEqual(response.status_code, 200)


class TestBuildersResource(TestCaseMixin):
    def setUp(self):
        pass

    def tearDown(self):
        Builder.destroy(*Builder.all().pluck('id').all())

    def test_get(self):
        self.factory(Builder, 200).create()
        response = self.client.get('/api/builders')
        self.assertEquals(len(response.get_json()), 200)

    def test_post(self):
        data = self.factory(Builder).make().to_json()
        response = self.client.post(
            '/api/builders',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()
        
        for key, value in self.json.loads(data).items():
            self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))

