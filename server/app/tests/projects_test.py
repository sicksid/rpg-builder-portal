from app.tests import TestCaseMixin
from app.models import Project


class TestProjectResource(TestCaseMixin):
    def setUp(self):
        self.project = self.factory(Project).create()

    def tearDown(self):
        Project.destroy(Project.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/projects/{self.project.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'name': 'name changed',
            'description': 'new description'
        }
        response = self.client.put(
            f"/api/projects/{self.project.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        project = Project.find(self.project.id)
        self.assertEqual(project.name, data['name'])
        self.assertEqual(project.description, data['description'])
        self.assertEqual(response.status_code, 200)


class TestProjectsResource(TestCaseMixin):
    maxDiff = None

    def tearDown(self):
        Project.destroy(*Project.all().pluck('id').all())

    def test_get(self):
        self.factory(Project, 100).create()
        response = self.client.get('/api/projects')
        self.assertEquals(len(response.get_json()), 100)

    def test_post(self):
        project = self.factory(Project).make().serialize()
        data = self.json.dumps(project)
        response = self.client.post(
            '/api/projects',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()
        for key, value in self.json.loads(data).items():
                self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
