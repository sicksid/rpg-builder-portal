from app.tests import TestCaseMixin
from app.models import Spec, Lot, Plan, Elevation


class SpecTestCase(TestCaseMixin):
    def setUp(self):
        pass

    def test_lot(self):
        spec = self.factory(Spec).create()
        lot = self.factory(Lot).create()
        lot.spec_id = spec.id
        lot.save()

        lot = Lot.find(lot.id)
        spec = Spec.find(spec.id)
        self.assertEqual(lot.spec.id, spec.id)
        self.assertIsNotNone(spec.lot)
        self.assertIsNotNone(lot.spec)


class TestSpecResource(TestCaseMixin):
    def setUp(self):
        self.spec = self.factory(Spec).create()

    def tearDown(self):
        Spec.destroy(Spec.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/specs/{self.spec.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'price': 1000000,
            'sqft': 2900
        }
        response = self.client.put(
            f"/api/specs/{self.spec.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        spec = Spec.find(self.spec.id)
        self.assertEqual(spec.price, data['price'])
        self.assertEqual(spec.sqft, data['sqft'])
        self.assertEqual(response.status_code, 200)


class TestSpecsResource(TestCaseMixin):
    maxDiff = None

    def tearDown(self):
        Spec.destroy(*Spec.all().pluck('id').all())

    def test_get(self):
        self.factory(Spec, 100).create()
        response = self.client.get('/api/specs')
        self.assertEquals(len(response.get_json()), 100)
    
    def test_post(self):
        spec = self.factory(Spec).make().serialize()
        plan = self.factory(Plan).make().serialize()
        elevation = self.factory(Elevation).make().serialize()
        spec['plan'] = plan
        spec['elevation'] = elevation
        data = self.json.dumps(spec)
        response = self.client.post(
            '/api/specs',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()
        for key, value in self.json.loads(data).items():
            if key in ['plan', 'elevation']:
                del response_data[key]['id']
                del response_data[key]['created_at']
                del response_data[key]['updated_at']
                self.assertDictEqual(response_data[key], value)
            else:
                self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
