from app.tests import TestCaseMixin
from app.models import Elevation, Plan


# class PlanTestCase(TestCaseMixin):
#     def setUp(self):
#         pass

#     def test_add_materials(self):
#         from ..models import Plan, Material
#         plan = self.factory(Plan).create()
#         material = self.factory(Material).create()

#         plan.materials().save(material)
#         self.assertEquals(plan.materials().get_results().count(), 1)


class TestPlanResource(TestCaseMixin):
    def setUp(self):
        self.plan = self.factory(Plan).create()

    def tearDown(self):
        Plan.destroy(Plan.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/plans/{self.plan.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'description': 'TEST Description',
            'name': 'Walsh'
        }
        response = self.client.put(
            f"/api/plans/{self.plan.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        plan = Plan.find(self.plan.id)
        self.assertEqual(plan.name, data['name'])
        self.assertEqual(plan.description, data['description'])
        self.assertEqual(response.status_code, 200)


class TestPlansResource(TestCaseMixin):
    def setUp(self):
        pass

    def tearDown(self):
        Plan.destroy(*Plan.all().pluck('id').all())

    def test_get(self):
        self.factory(Plan, 100).create()
        response = self.client.get('/api/plans')
        self.assertEquals(len(response.get_json()), 100)

    def test_post(self):
        data = self.factory(Plan).make().to_json()
        response = self.client.post(
            '/api/plans',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()

        for key, value in self.json.loads(data).items():
            self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
