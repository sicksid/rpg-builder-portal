from app.tests import TestCaseMixin
from app.lib import CSVManager
from app.models import Lot
# from ..factories import LotFactory change this for orator stuff
# from ..models import Lot


class TestCSVManager(TestCaseMixin):
    def setUp(self):
        self.factory(Lot, 10).create()
    
    def tearDown(self):
        Lot.destroy(Lot.all().pluck('id').all())

    def test_initialize(self):
        class FakeModel(object):
            pass
        csv_manager = CSVManager(FakeModel)
        self.assertEqual(csv_manager.model.__name__, FakeModel.__name__)

    def test_initialize_without_model(self):
        self.assertRaises(Exception, CSVManager)

    def test_model_property(self):
        class FakeModel(object):
            pass

        csv_manager = CSVManager(FakeModel)
        self.assertEqual(csv_manager.model.__name__, FakeModel.__name__)

    def test_model_setter(self):
        class FakeModel(object):
            pass

        csv_manager = CSVManager(FakeModel)

        class SecondFakeModel(FakeModel):
            pass

        csv_manager.model = SecondFakeModel
        self.assertEqual(csv_manager.model.__name__, SecondFakeModel.__name__)

    def test_validate_initialized(self):
        class FakeModel(object):
            pass

        csv_manager = CSVManager(FakeModel)
        csv_manager.model = None
        self.assertRaises(Exception, csv_manager.validate_initialized)

    def test_unload(self):
        Lot.destroy(Lot.all().pluck('id').all())
        self.factory(Lot, 10).create()

        csv_manager = CSVManager(Lot)
        csv = csv_manager.unload(['id', 'address'], None)

        self.assertEqual(len(csv), 11) # 11 instead of 10 because of the headers

        for lot in Lot.all():
            lot.delete()

    def test_load(self):
        csv_manager = CSVManager(Lot)
        csv = csv_manager.unload(['id', 'address'], None)

        old_csv = csv

        csv = []
        for index, row in enumerate(old_csv):
            if index == 0:
                csv.append(row)
                continue
            csv.append([row[0], 'TEST'])

        csv_manager.load(['address'], csv)
        for lot in Lot.all():
            self.assertEqual(lot.address, 'TEST')
            self.assertNotEqual(lot.id, 'TEST')
