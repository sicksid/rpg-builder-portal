from app.tests import TestCaseMixin
from app.models import Lot


class TestLotResource(TestCaseMixin):
    def setUp(self):
        self.lot = self.factory(Lot).create()

    def tearDown(self):
        Lot.destroy(*Lot.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/lots/{self.lot.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'address': 'TEST ADDRESS',
            'block': 'B'
        }
        response = self.client.put(
            f"/api/lots/{self.lot.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        lot = Lot.find(self.lot.id)
        self.assertEqual(lot.address, data['address'])
        self.assertEqual(lot.block, data['block'])
        self.assertEqual(response.status_code, 200)


class TestLotsResource(TestCaseMixin):
    def tearDown(self):
        Lot.destroy(Lot.all().pluck('id').all())

    def test_get(self):
        self.factory(Lot, 200).create()
        response = self.client.get('/api/lots')
        self.assertEquals(len(response.get_json()), 200)


    def test_post(self):
        data = self.factory(Lot).make().to_json()
        response = self.client.post(
            '/api/lots',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()
        
        for key, value in self.json.loads(data).items():
            if key == 'sku':
                value = value.replace('-', '')

            self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
