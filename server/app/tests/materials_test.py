from app.tests import TestCaseMixin
from app.models import Material


class TestMaterialResource(TestCaseMixin):
    def setUp(self):
        self.material = self.factory(Material).create()

    def tearDown(self):
        Material.destroy(Material.all().pluck('id').all())

    def test_get(self):
        response = self.client.get(f"/api/materials/{self.material.id}")
        self.assertEqual(response.status_code, 200)

    def test_put(self):
        data = {
            'category': 'TEST category',
            'name': 'Walsh'
        }
        response = self.client.put(
            f"/api/materials/{self.material.id}",
            data=self.json.dumps(data),
            content_type='application/json'
        )

        material = Material.find(self.material.id)
        self.assertEqual(material.name, data['name'])
        self.assertEqual(material.category, data['category'])
        self.assertEqual(response.status_code, 200)


class TestMaterialsResource(TestCaseMixin):
    def setUp(self):
        pass

    def tearDown(self):
        Material.destroy(*Material.all().pluck('id').all())

    def test_get(self):
        self.factory(Material, 100).create()
        response = self.client.get('/api/materials')
        self.assertEquals(len(response.get_json()), 100)

    def test_post(self):
        data = self.factory(Material).make().to_json()
        response = self.client.post(
            '/api/materials',
            data=data,
            content_type='application/json'
        )

        self.assertTrue(response.status_code, 200)
        response_data = response.get_json()

        for key, value in self.json.loads(data).items():
            self.assertEqual(response_data[key], value)

        self.assertIsNotNone(response_data.get('id'))
        self.assertIsNotNone(response_data.get('created_at'))
        self.assertIsNotNone(response_data.get('updated_at'))
