from subprocess import call as execute
from tempfile import TemporaryDirectory
from os.path import abspath, basename
from os.path import split as path_split
from os.path import join as path_join


class DxfConverter(object):
    command = "ogr2ogr -overwrite -f \"Esri Shapefile\" -skipfailures \"{output}\" \"{input}\""

    def __init__(self, path):
        self.input = abspath(path)
        filename = basename(self.input)
        self.output_filename = filename.replace('dxf', 'shp')
        with TemporaryDirectory() as tempdir:
            output = path_join(tempdir, self.output_filename)
            result = execute(self.command.format(input=self.input, output=output), shell=True)
            if result == 0:
                self.output = open(output, 'rb')
                self.shp = self.output
                self.dbf = open(output.replace('.shp', '.dbf'), 'rb')
                self.shx = open(output.replace('.shp', '.shx'), 'rb')
            else:
                raise Exception("Error writing file")

    def store(self, path):
        shp_path = path_join(path, self.output_filename)
        shx_path = path_join(path, self.output_filename.replace('shp', 'shx'))
        dbf_path = path_join(path, self.output_filename.replace('shp', 'dbf'))
        with open(shp_path, 'wb') as f:
            self.shp.seek(0)
            f.write(self.shp.read())
            self.output.close()
            self.shp = None
            self.output = None

        with open(shx_path, 'wb') as f:
            self.shx.seek(0)
            f.write(self.shx.read())
            self.shx.close()
            self.shx = None

        with open(dbf_path, 'wb') as f:
            self.dbf.seek(0)
            f.write(self.dbf.read())
            self.dbf.close()
            self.dbf = None

        return {
            'shp': shp_path,
            'shx': shx_path,
            'dbf': dbf_path
        }
