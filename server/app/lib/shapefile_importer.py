# from shapely.geometry import Polygon, shape
from pyproj import Proj, transform, itransform
import shapefile


class ShapefileImporter(object):
    def __init__(self, path):
        self.reader = shapefile.Reader(path)
        self.errors = []
        self.fields = [field[0] for field in self.reader.fields[1:]]

    @property
    def data(self):
        data = dict(
            type="FeatureCollection",
            features=list(filter(lambda feature: feature, [
                self.generate_feature(feature)
                for feature in self.extract_lots(self.reader.shapeRecords())
            ]))
        )
        return data

    def transform_coords(self, coordinates):
        return list(itransform(Proj(init='epsg:86'), Proj(init='epsg:4326'), coordinates))

    def extract_lots(self, features):
        return list(filter(lambda feature: dict(zip(self.fields, feature.record)).get('Layer') == 'LOT SHAPES', features))

    def generate_feature(self, feature):
        properties = dict(zip(self.fields, feature.record))
        try:
            geometry = feature.shape.__geo_interface__
            coordinates = [coords  for coords in geometry['coordinates']]
            geometry['type'] = 'Polygon'
            geometry['coordinates'] = [self.transform_coords(coordinates)]
        except Exception as exception:
            print(exception)
            self.errors.append(exception)
            return None

        return dict(
            type="Feature",
            geometry=geometry,
            properties=properties
        )
