import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Material
from app.schemas import MaterialSchema


class MaterialResource(ResourceMixin):
    @marshal_with(MaterialSchema)
    def get(self, material_id):
        try:
            return Material.find_or_fail(material_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(MaterialSchema(strict=True))
    @marshal_with(MaterialSchema(strict=True))
    def put(self, material_id, **kwargs):
        return Material.where('id', material_id).update(kwargs)


class MaterialsResource(ResourceMixin):
    @marshal_with(MaterialSchema(strict=True, many=True))
    def get(self):
        return Material.all()

    @use_kwargs(MaterialSchema(strict=True))
    @marshal_with(MaterialSchema(strict=True), code=201)
    def post(self, **kwargs):
        return Material.create(**kwargs).serialize()
