import json, os
from tempfile import TemporaryDirectory
from flask_restful import abort
from flask import request
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.lib import DxfConverter, ShapefileImporter


class DXFResource(ResourceMixin):
    def post(self):
      dxf = request.files.get('dxf')
      with TemporaryDirectory() as temporary_directory:
        dxf_path = os.path.join(temporary_directory, 'temporary_file.dxf')
        dxf.save(dxf_path)
        dxf_converter = DxfConverter(dxf_path)
        shapefiles = dxf_converter.store(temporary_directory)
        shapefile_importer = ShapefileImporter(shapefiles.get('shp'))
        geojson = shapefile_importer.data
        return geojson
