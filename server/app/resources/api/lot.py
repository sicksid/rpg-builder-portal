import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.schemas import LotSchema
from app.resources.api.utils import ResourceMixin
from app.models import Lot


class LotResource(ResourceMixin):

    @marshal_with(LotSchema)
    def get(self, lot_id):
        try:
            return Lot.find_or_fail(lot_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(LotSchema(strict=True))
    @marshal_with(LotSchema(strict=True))
    def put(self, lot_id, **kwargs):
        return Lot.where('id', lot_id).update(kwargs)

class LotsResource(ResourceMixin):
    @marshal_with(LotSchema(strict=True, many=True))
    def get(self):
        return Lot.all()

    @use_kwargs(LotSchema(strict=True))
    @marshal_with(LotSchema(strict=True), code=201)
    def post(self, **kwargs):
        kwargs['sku'] = kwargs['sku'].hex
        kwargs['length'] = int(kwargs['length'])
        kwargs['size'] = int(kwargs['size'])
        return Lot.create(**kwargs).serialize()

