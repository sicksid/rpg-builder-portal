import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Project
from app.schemas import ProjectSchema


class ProjectResource(ResourceMixin):
    @marshal_with(ProjectSchema)
    def get(self, project_id):
        try:
            return Project.find_or_fail(project_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(ProjectSchema(strict=True))
    @marshal_with(ProjectSchema(strict=True))
    def put(self, project_id, **kwargs):
        project = Project.where('id', project_id)
        return project.update(kwargs)


class ProjectsResource(ResourceMixin):
    @use_kwargs(ProjectSchema(strict=True))
    @marshal_with(ProjectSchema(strict=True, many=True))
    def get(self, **kwargs):
        return Project.all()

    @use_kwargs(ProjectSchema(strict=True))
    @marshal_with(ProjectSchema(strict=True), code=201)
    def post(self, **kwargs):
        return Project.create(**kwargs).serialize()
