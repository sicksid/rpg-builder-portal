import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Elevation, Plan, Style
from app.schemas import ElevationSchema


class ElevationResource(ResourceMixin):
    @marshal_with(ElevationSchema)
    def get(self, elevation_id):
        try:
            return Elevation.find_or_fail(elevation_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(ElevationSchema(strict=True))
    @marshal_with(ElevationSchema(strict=True))
    def put(self, elevation_id, **kwargs):
        return Elevation.where('id', elevation_id).update(kwargs)


class ElevationsResource(ResourceMixin):
    @marshal_with(ElevationSchema(strict=True, many=True))
    def get(self):
        return Elevation.all()

    @use_kwargs(ElevationSchema(strict=True))
    @marshal_with(ElevationSchema(strict=True), code=201)
    def post(self, **kwargs):
        plan = Plan.create(**kwargs.pop('plan'))
        style = Style.create(**kwargs.pop('style'))
        kwargs['plan_id'] = plan.id
        kwargs['style_id'] = style.id
        elevation = Elevation.create(**kwargs).serialize()
        elevation['plan'] = plan.serialize()
        elevation['style'] = style.serialize()
        return elevation
