from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import MarketingTraffic
from app.schemas import MarketingTrafficSchema


class MarketingTrafficResource(ResourceMixin):

    @marshal_with(MarketingTrafficSchema)
    def get(self, marketing_traffic_id):
        try:
            return MarketingTraffic.find_or_fail(marketing_traffic_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(MarketingTrafficSchema(strict=True))
    @marshal_with(MarketingTrafficSchema(strict=True))
    def put(self, marketing_traffic_id, **kwargs):
        return MarketingTraffic.where('id', marketing_traffic_id).update(kwargs)


class MarketingTrafficsResource(ResourceMixin):

    @marshal_with(MarketingTrafficSchema(strict=True, many=True))
    def get(self):
        return MarketingTraffic.all()

    @use_kwargs(MarketingTrafficSchema(strict=True))
    @marshal_with(MarketingTrafficSchema(strict=True), code=201)
    def post(self, **kwargs):
        return MarketingTraffic.create(**kwargs).serialize()
