from flask import Blueprint
from flask_restful import Api

api_blueprint = Blueprint('api', __name__)
flask_api = Api(api_blueprint)


from app.resources.api.lot import LotResource, LotsResource
from app.resources.api.builder import BuilderResource, BuildersResource
#   from .buyer import BuyerResource, BuyersResource
#   from .developer import DeveloperResource, DevelopersResource
from app.resources.api.elevation import ElevationResource, ElevationsResource
#   from .marketing_traffic import MarketingTrafficResource, MarketingTrafficsResources
from app.resources.api.marketing_traffic import MarketingTrafficResource, MarketingTrafficsResource
from app.resources.api.sale import SaleResource, SalesResource
from app.resources.api.plan import PlanResource, PlansResource
from app.resources.api.spec import SpecResource, SpecsResource
from app.resources.api.material import MaterialResource, MaterialsResource
from app.resources.api.project import ProjectResource, ProjectsResource
from app.resources.api.dxf import DXFResource
from app import docs


# Lots
flask_api.add_resource(LotResource, '/lots/<lot_id>')
docs.register(LotResource, blueprint='api')
flask_api.add_resource(LotsResource, '/lots')
docs.register(LotsResource, blueprint='api')

# Builders
flask_api.add_resource(BuilderResource, '/builders/<builder_id>')
docs.register(BuilderResource, blueprint='api')
flask_api.add_resource(BuildersResource, '/builders')
docs.register(BuildersResource, blueprint='api')

# Elevation
flask_api.add_resource(ElevationResource, '/elevations/<elevation_id>')
docs.register(ElevationResource, blueprint='api')
flask_api.add_resource(ElevationsResource, '/elevations')
docs.register(ElevationsResource, blueprint='api')

# Marketing Traffic
flask_api.add_resource(MarketingTrafficResource,
                    '/reports/marketing/<marketing_traffic_id>')
docs.register(MarketingTrafficResource, blueprint='api')
flask_api.add_resource(MarketingTrafficsResource, '/reports/marketing')
docs.register(MarketingTrafficsResource, blueprint='api')

# Sales
flask_api.add_resource(SaleResource, '/sales/<sale_id>')
docs.register(SaleResource, blueprint='api')
flask_api.add_resource(SalesResource, '/sales')
docs.register(SalesResource, blueprint='api')

# Plan
flask_api.add_resource(PlanResource, '/plans/<plan_id>')
docs.register(PlanResource, blueprint='api')
flask_api.add_resource(PlansResource, '/plans')
docs.register(PlansResource, blueprint='api')

# Spec
flask_api.add_resource(SpecResource, '/specs/<spec_id>')
docs.register(SpecResource, blueprint='api')
flask_api.add_resource(SpecsResource, '/specs')
docs.register(SpecsResource, blueprint='api')

# Material
flask_api.add_resource(MaterialResource, '/materials/<material_id>')
docs.register(MaterialResource, blueprint='api')
flask_api.add_resource(MaterialsResource, '/materials')
docs.register(MaterialsResource, blueprint='api')

# Project
flask_api.add_resource(ProjectResource, '/projects/<project_id>')
docs.register(ProjectResource, blueprint='api')
flask_api.add_resource(ProjectsResource, '/projects')
docs.register(ProjectsResource, blueprint='api')

# DXF
flask_api.add_resource(DXFResource, '/dxf')
docs.register(DXFResource, blueprint='api')