import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Plan
from app.schemas import PlanSchema


class PlanResource(ResourceMixin):
    @marshal_with(PlanSchema)
    def get(self, plan_id):
        try:
            return Plan.find_or_fail(plan_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(PlanSchema(strict=True))
    @marshal_with(PlanSchema(strict=True))
    def put(self, plan_id, **kwargs):
        return Plan.where('id', plan_id).update(kwargs)


class PlansResource(ResourceMixin):
    @marshal_with(PlanSchema(strict=True, many=True))
    def get(self):
        return Plan.all()

    @use_kwargs(PlanSchema(strict=True))
    @marshal_with(PlanSchema(strict=True), code=201)
    def post(self, **kwargs):
        return Plan.create(**kwargs).serialize()
