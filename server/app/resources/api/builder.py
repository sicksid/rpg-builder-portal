import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.schemas import BuilderSchema
from app.models import Builder


class BuilderResource(ResourceMixin):
    @marshal_with(BuilderSchema)
    def get(self, builder_id):
        from app.models import Builder
        try:
            return Builder.find_or_fail(builder_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(BuilderSchema(strict=True))
    @marshal_with(BuilderSchema(strict=True))
    def put(self, builder_id, **kwargs):
        from app.models import Builder
        return Builder.where('id', builder_id).update(kwargs)

class BuildersResource(ResourceMixin):
    @marshal_with(BuilderSchema(strict=True, many=True))
    def get(self):
        from app.models import Builder
        return Builder.all()

    @use_kwargs(BuilderSchema(strict=True))
    @marshal_with(BuilderSchema(strict=True), code=201)
    def post(self, **kwargs):
        from app.models import Builder
        return Builder.create(**kwargs).serialize()
