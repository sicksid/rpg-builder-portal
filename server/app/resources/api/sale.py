import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Sale, Realtor, Buyer
from app.schemas import SaleSchema


class SaleResource(ResourceMixin):
    @marshal_with(SaleSchema)
    def get(self, sale_id):
        try:
            return Sale.find_or_fail(sale_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(SaleSchema(strict=True))
    @marshal_with(SaleSchema(strict=True))
    def put(self, sale_id, **kwargs):
        sale = Sale.where('id', sale_id)
        if kwargs.get('is_closed'):
            sale.first().close()
        elif kwargs.get('is_cancelled'):
            sale.first().cancel()

        return sale.update(kwargs)


class SalesResource(ResourceMixin):
    @use_kwargs(SaleSchema(strict=True))
    @marshal_with(SaleSchema(strict=True, many=True))
    def get(self, **kwargs):
        if kwargs.get('is_closed'):
            return Sale.closings().get()
        elif kwargs.get('is_cancelled'):
            return Sale.cancellations().get()

        return Sale.all()

    @use_kwargs(SaleSchema(strict=True))
    @marshal_with(SaleSchema(strict=True), code=201)
    def post(self, **kwargs):
        realtor = Realtor.create(**kwargs.pop('realtor'))
        buyer = Buyer.create(**kwargs.pop('buyer'))
        kwargs['realtor_id'] = realtor.id
        kwargs['buyer_id'] = buyer.id
        sale = Sale.create(**kwargs).serialize()
        sale['realtor'] = realtor.serialize()
        sale['buyer'] = buyer.serialize()
        return sale
