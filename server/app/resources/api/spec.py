import json
from flask_restful import abort
from flask import request
from flask_orator import ModelNotFound
from flask_apispec import use_kwargs, marshal_with
from app.resources.api.utils import ResourceMixin
from app.models import Spec, Elevation, Plan
from app.schemas import SpecSchema


class SpecResource(ResourceMixin):
    @marshal_with(SpecSchema)
    def get(self, spec_id):
        try:
            return Spec.find_or_fail(spec_id)
        except ModelNotFound:
            abort(404, message="Record Not Found")

    @use_kwargs(SpecSchema(strict=True))
    @marshal_with(SpecSchema(strict=True))
    def put(self, spec_id, **kwargs):
        spec = Spec.where('id', spec_id)
        return spec.update(kwargs)


class SpecsResource(ResourceMixin):
    @use_kwargs(SpecSchema(strict=True))
    @marshal_with(SpecSchema(strict=True, many=True))
    def get(self, **kwargs):
        return Spec.all()

    @use_kwargs(SpecSchema(strict=True))
    @marshal_with(SpecSchema(strict=True), code=201)
    def post(self, **kwargs):
        plan = Plan.create(**kwargs['plan'])
        elevation = Elevation.create(**kwargs['elevation'])
        kwargs['plan_id'] = plan.id
        kwargs['elevation_id'] = elevation.id
        del kwargs['plan']
        del kwargs['elevation']
        spec = Spec.create(**kwargs).serialize()
        spec['plan'] = plan.serialize()
        spec['elevation'] = elevation.serialize()
        return spec
