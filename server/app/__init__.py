import os, calendar
from flask import Flask, render_template, send_file
from flask_webpack import Webpack
from flask_apispec import FlaskApiSpec
from flask_orator import Orator
from orator.orm import Factory

db = Orator()
factory = Factory()
docs = FlaskApiSpec()
#webpack = Webpack()

def create_app():
    from app.config import config
    from app.resources.api import api_blueprint
    flask_app = Flask(__name__)
    environment = os.environ['FLASK_ENV']
    config_object = config[environment]

    if environment == 'production':
        config_object['ORATOR_DATABASES'] = {
            'default': 'production',
            'production': {
                'driver': 'mysql',
                'host': os.environ.get('DATABASE_HOST'),
                'database': os.environ.get('DATABASE_NAME'),
                'user': os.environ.get('DATABASE_USER'),
                'password': os.environ.get('DATABASE_PASSWORD')
            }
        }

    flask_app.config.from_object(config_object)
    flask_app.static_url_path = flask_app.config.get('STATIC_FOLDER')
    flask_app.static_folder = flask_app.root_path.replace('/api', '') + flask_app.static_url_path
    config_object.init_app(flask_app)
    flask_app.register_blueprint(api_blueprint, url_prefix='/api')
    db.init_app(flask_app)
    docs.init_app(flask_app)

    @flask_app.route('/builds/<file>')
    def builds(file):
        return send_file('/home/deployer/client/dist/' + file) 
    
    @flask_app.route('/')
    def index():
        return render_template('index.jinja2')

    return flask_app
