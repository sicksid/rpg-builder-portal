from orator.migrations import Migration


class AddProjectIdToModels(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('specs') as table:
            table.integer('project_id').unsigned().nullable()
            table.foreign('project_id').references('id').on('projects').nullable()

        with self.schema.table('plans') as table:
            table.integer('project_id').unsigned().nullable()
            table.foreign('project_id').references(
                'id').on('projects').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('specs') as table:
            table.drop_column('project_id')

        with self.schema.table('plans') as table:
            table.drop_column('project_id')
