from orator.migrations import Migration


class CreateSpecsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('specs') as table:
            table.increments('id')
            table.timestamps()
            table.big_integer('price')
            table.integer('sqft')
            table.boolean('front_porch')
            table.long_text('description').nullable()
            table.date('available_on').nullable()
            table.boolean('is_arc').default(0)
            table.enum('status', ['approved', 'denied', 'pending']).default('pending')
            table.date('approved_on').nullable()
            table.date('denied_on').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('specs')
