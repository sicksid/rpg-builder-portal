from orator.migrations import Migration


class CreateLotsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('lots') as table:
            table.increments('id')
            table.timestamps()
            table.string('address')
            table.string('sku')
            table.long_text('description').nullable()
            table.string('street')
            table.string('street_block')
            table.enum('status', [
                'available',
                'sold',
                'spec',
                'reserved',
                'closed',
                'arc',
                'coming soon'
            ])
            table.enum('type', [
                'build',
                'move in',
                'construction',
                'window',
                'model',
                'occupied',
                'vacant',
                'trailer',
                'reserved',
                'vacant lot'
            ])
            table.string('block')
            table.boolean('custom')
            table.decimal('size', 9, 2)
            table.decimal('length', 9, 2)

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('lots')
