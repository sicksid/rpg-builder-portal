from orator.migrations import Migration


class AddSaleToLotsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('lots') as table:
            table.integer('sale_id').unsigned().nullable()
            table.foreign('sale_id').references('id').on('sales')

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('lots') as table:
            table.drop_columns('sale_id')
