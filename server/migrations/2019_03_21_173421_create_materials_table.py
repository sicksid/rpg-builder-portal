from orator.migrations import Migration


class CreateMaterialsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('materials') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.string('category')
            table.string('manufacturer')
            table.enum('status', ['approved', 'denied', 'pending']).default('pending')

            table.integer('uploaded_by_id').unsigned().nullable()
            table.foreign('uploaded_by_id').references('id').on('users')
            table.enum('type', [
                'roofing',
                'siding',
                'secondary'
            ])


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('materials')
