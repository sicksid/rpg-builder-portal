from orator.migrations import Migration


class CreateBuildersMaterialsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('builders_materials') as table:
            table.increments('id')
            table.timestamps()
            table.integer('builder_id').unsigned()
            table.integer('material_id').unsigned()
            table.foreign('builder_id').references('id').on('builders')
            table.foreign('material_id').references('id').on('materials')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('builders_materials')
