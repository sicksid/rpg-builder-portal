from orator.migrations import Migration


class CreateSalesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('sales') as table:
            table.increments('id')
            table.timestamps()
            table.big_integer('price')
            table.integer('sqft').nullable()
            table.integer('created_by_id').unsigned().nullable()
            table.integer('updated_by_id').unsigned().nullable()
            table.integer('project_id').unsigned().nullable()
            table.integer('buyer_id').unsigned().nullable()
            table.integer('realtor_id').unsigned().nullable()
            table.foreign('created_by_id').references('id').on('users').nullable()
            table.foreign('updated_by_id').references('id').on('users').nullable()
            table.foreign('project_id').references(
                'id').on('projects').nullable()
            table.foreign('buyer_id').references('id').on('buyers').nullable()
            table.foreign('realtor_id').references('id').on('realtors').nullable()
            table.boolean('is_closed').default(0)
            table.boolean('is_cancelled').default(0)
            table.date('sold_on').nullable()
            table.date('closed_on').nullable()
            table.date('cancelled_on').nullable()
            table.date('estimated_close_on').nullable()


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('sales')
