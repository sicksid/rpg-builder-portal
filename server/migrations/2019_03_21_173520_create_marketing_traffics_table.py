from orator.migrations import Migration


class CreateMarketingTrafficsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('marketing_traffics') as table:
            table.increments('id')
            table.timestamps()
            table.integer('weekday_visitors').nullable()
            table.integer('weekend_visitors').nullable()
            table.date('start_on')
            table.date('end_on')
            table.integer('created_by_id').unsigned().nullable()
            table.integer('project_id').unsigned().nullable()
            table.foreign('project_id').references('id').on('projects')
            table.foreign('created_by_id').references('id').on('users')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('marketing_traffics')
