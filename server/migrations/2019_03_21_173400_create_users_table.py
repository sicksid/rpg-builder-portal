from orator.migrations import Migration


class CreateUsersTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('users') as table:
            table.increments('id')
            table.timestamps()
            table.string('email')
            table.string('second_email').nullable()
            table.string('first_name')
            table.string('last_name')
            table.string('phone').nullable()
            table.string('title').nullable()
            table.integer('active_project_id').unsigned()
            table.foreign('active_project_id').references('id').on('projects')
            table.string('recovery_token').nullable()
            table.boolean('verified')
            table.datetime('verified_at')
            table.json('schedule')
            table.json('settings')
            table.enum('role', [
                'admin',
                'consultant',
                'community manager',
                'investor',
                'executive',
                'partner',
                'staff',
                'super user'
            ])


    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('users')
