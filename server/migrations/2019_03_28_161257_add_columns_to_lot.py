from orator.migrations import Migration


class AddColumnsToLot(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('lots') as table:
            table.integer('project_id').unsigned().nullable()
            table.foreign('project_id').references(
                'id').on('projects').nullable()
            table.json('polygon').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('lots') as table:
            table.drop_column('project_id')
            table.drop_column('polygon')
