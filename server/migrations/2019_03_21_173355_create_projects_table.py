import json
from orator.migrations import Migration

config = {
    "map_center": None,
    "mapbox_access_token": None,
    "map_zoom": None,
    "map_style": None,
    "acknowledgement": None,
    "arc_approval_message": None,
}

class CreateProjectsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('projects') as table:
            table.increments('id')
            table.timestamps()
            table.string('name')
            table.long_text('description').nullable()
            table.json('config').default(json.dumps(config))

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('projects')
