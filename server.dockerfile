FROM ubuntu:latest
ENV LANG C.UTF-8
ENV TZ=America/Mexico_City
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN useradd -ms /bin/bash deployer 
RUN usermod -aG sudo deployer
RUN apt-get -yy update && \
    apt-get -y install gdal-bin libgdal-dev python3-pip python3-dev 
RUN cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
ENV HOME /home/deployer
RUN mkdir $HOME/server
RUN chown -R deployer:deployer $HOME
RUN chown -R deployer:deployer /tmp 
USER deployer 
WORKDIR /home/deployer/server
ENV CPLUS_INCLUDE_PATH /usr/include/gdal
ENV C_INCLUDE_PATH /usr/include/gdal
ADD ./server ./ 
RUN pip3 install -r requirements.txt --user
ENV FLASK_APP server
ENV FLASK_ENV development
RUN python3 shell.py migrate -f
CMD python3 wsgi.py
