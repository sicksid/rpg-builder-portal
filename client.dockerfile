FROM node:10.15.3-slim
RUN useradd -ms /bin/bash deployer 
RUN usermod -aG sudo deployer
ENV HOME /home/deployer
WORKDIR /home/deployer/client
RUN mkdir dist
ADD ./client ./
RUN yarn install
RUN yarn global add webpack webpack-cli
# RUN chown -R deployer:deployer $HOME
CMD yarn start
