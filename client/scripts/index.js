import 'mapbox-gl/dist/mapbox-gl.css'
import ReactDOM from 'react-dom'
import React from 'react'
import { createStore } from 'redux'
import { StoreContext } from 'redux-react-hook'
import generalReducer from './store/reducers'
import Home from './pages/Home'
import Sales from './pages/Sales'
import Map from './components/Map'
import { BrowserRouter as Router, Route } from 'react-router-dom'

const rootElement = document.getElementById('web-application')
const store = createStore(generalReducer)

function App() {
  console.log('dev')
  return (
    <StoreContext.Provider value={store}>
      <Router>
        <div className="row" style={{margin: '0'}}>
          <div className="col-3" style={{ maxHeight: '100vh', overflow: 'hidden', overflowY: 'auto' }}>
            <Route exact path="/" component={Home} />
            <Route exact path="/sales" component={Sales} />
          </div>
          <div id="map-container" className="col-9" style={{
            height: '100vh'
          }}>
            <Map />
          </div>
        </div>
      </Router>
    </StoreContext.Provider>
  )
}

ReactDOM.render(
  <App />,
  rootElement
)
