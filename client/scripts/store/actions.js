export const POPULATE = 'POPULATE'
export const SELECT = 'SELECT'
export const TRANSLATE = 'TRANSLATE'
export const SET_TRANSLATE_TO = 'SET_TRANSLATE_TO'
export const SET_CENTERPOINT = 'SET_CENTERPOINT'

export function populateRecords(payload) {
  return {
    type: POPULATE,
    payload
  }
}

export function selectRecord(payload) {
  return {
    type: SELECT,
    payload
  }
}

export function translateRecord(record, { coordinates }) {
  return {
    type: TRANSLATE,
    payload: { record, coordinates }
  }
}

export function setTranslateTo(payload) {
  return {
    type: SET_TRANSLATE_TO,
    payload
  }
}

export function setCenterpoint(payload) {
  return {
    type: SET_CENTERPOINT,
    payload
  }
}