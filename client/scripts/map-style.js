import { fromJS } from 'immutable';
import MAP_STYLE from './map-style-basic-v8.json';

// For more information on data-driven styles, see https://www.mapbox.com/help/gl-dds-ref/
export const dataLayer = fromJS({
  id: 'data',
  source: 'records',
  type: 'fill',
  paint: {
    'fill-color': 'white',
    'fill-opacity': 1,
    'fill-outline-color': 'black'
  }
});

export const selectedLayer = fromJS({
  id: "hover",
  source: "records",
  type: "fill",
  paint: {
    "fill-color": "#181F35",
    "fill-opacity": 1,
    "fill-outline-color": "#181F35"
  },
  filter: ["==", "id", ""]
})

export const defaultMapStyle = fromJS(MAP_STYLE);