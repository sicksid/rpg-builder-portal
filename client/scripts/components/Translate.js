import React, { useCallback } from 'react'
import { useMappedState, useDispatch } from 'redux-react-hook'
import { translateRecord, setCenterpoint } from '../store/actions'
import { distance, transformTranslate, point, centerOfMass } from '@turf/turf'
import { isPolygon, isPolygonCoor } from 'geojson-validation'


export default function Translate() {
  const mapState = useCallback(state => ({
    selected: state.selected,
    records: state.records,
    translate_to: state.translate_to
  }))
  const { selected, translate_to, records } = useMappedState(mapState)
  const dispatch = useDispatch()
  const execute = useCallback((record, coordinates) => dispatch(translateRecord(record, coordinates)))
  const flyTo = useCallback(coordinates => dispatch(setCenterpoint(coordinates)))
  const polygon_obj = (records.features.find(feature => feature.properties.id == selected.id) || {}).geometry
  function translateHandler () {
    const [x1, y1] = eval(selected.centerpoint).reverse()
    const [x2, y2] = translate_to
    const direction = (Math.atan2((y2 - y1), (x2 - x1)) * (180 / Math.PI) + 360) % 360
    const new_distance = distance(point([x1, y1]), point([x2, y2]), { units: 'degrees' })
    const new_polygon = transformTranslate(polygon_obj, new_distance, direction, { units: 'degrees' })
    const new_centerpoint = centerOfMass(new_polygon)
    flyTo(new_centerpoint.geometry.coordinates)
    
    // isPolygon(new_polygon, (valid, errors) => {
    //   if (!valid) {
    //     console.log(errors)
    //   }
    // })
    // isPolygonCoor(new_polygon.coordinates[0], (valid, errors) => {
    //   if (!valid) {
    //     console.log(errors)
    //   }
    // })
    execute(selected, new_polygon)
  }
  const button = selected && translate_to ? (
    <button onClick={translateHandler} className="btn btn-primary">Translate</button>
  ) : undefined

  return (
    <div>
      <span>{translate_to.join(' ')}</span>
      {button}
    </div>
  )
}