import React, { useCallback } from 'react'
import { selectRecord } from '../store/actions'
import { useMappedState, useDispatch } from 'redux-react-hook'


export default function RecordProperties() {
  const mapState = useCallback(state => ({ selected: state.selected }))
  const { selected } = useMappedState(mapState)
  const dispatch = useDispatch()
  const remove = useCallback(_ => dispatch(selectRecord()))
  const label = selected ? selected.label : undefined
  return (
    <div className="card">
      <div className="card-header">
        <div className="row">
          <div className="col-10">
            <h6>{label}</h6>
          </div>
          <div className="col-2">
            <button className="btn btn-default" onClick={_ => remove()}>X</button>
          </div>
        </div>
      </div>
      <div className="card-body">
        <ul className="list-group">
          {Object.keys(selected).filter(key => key != 'id').map((key, index) => (
            <li className="list-group-item" key={index}>
              <div className="row">
                <div className="col">
                  <span>{key}</span>
                </div>
                <div className="col">
                  <span>{key == 'centerpoint' ? eval(selected[key]).join(' ') : selected[key]}</span>
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}