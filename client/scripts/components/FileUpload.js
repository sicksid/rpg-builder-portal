import React, { useState, useCallback, useRef } from 'react'
import { useDispatch } from 'redux-react-hook'
import Spinner from './Spinner'
import { populateRecords } from '../store/actions'


export default function FileUpload() {
  const [file, setFile] = useState(null)
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const fileInput = useRef(null)
  const populate = useCallback(records => dispatch(populateRecords(records)))

  function clickHandler() {
    const formData = new FormData()
    formData.append('dxf', file)
    setLoading(true)
    fetch('/api/dxf', {
      method: 'POST',
      body: formData
    }).then(response => response.json()).then(response => {
      setLoading(false)
      setFile(null)
      fileInput.current.value = ''
      console.log(JSON.stringify(response))
      populate(response)
    }).catch(
      error => !setLoading(false) && alert(error)
    )
  }

  return (
    <div className="input-group mb-3">
      <div className="custom-file">
        <input
          type="file"
          className="custom-file-input"
          id="file-input"
          ref={fileInput}
          onInput={event => setFile(event.target.files[0])} />
        <label className="custom-file-label" htmlFor="file-input">{file ? file.name : 'Choose File'}</label>
      </div>
      <div className="input-group-prepend">
        <button
          className="input-group-text"
          disabled={!file || loading}
          onClick={_ => clickHandler()}
          style={{ borderLeft: '0' }}
        >{loading ? (
          <div>
            <span style={{ marginRight: '5px' }}>
              Processing
            </span>
            <Spinner />
          </div>
        ) : 'Upload'}</button>
      </div>
    </div>
  )
}
