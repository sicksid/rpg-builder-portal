import { featureCollection, center, point, polygon } from '@turf/turf'
import React, { useCallback, useEffect, useState, useRef } from 'react'
import ReactMapGL, { NavigationControl } from 'react-map-gl'
import { defaultMapStyle, dataLayer, selectedLayer } from '../map-style.js'
import { fromJS } from 'immutable'
import { useMappedState, useDispatch } from 'redux-react-hook'
import { populateRecords, selectRecord, setTranslateTo } from '../store/actions'
import GEOJSON from '../geojson2.json'

const MAPBOX_TOKEN = "pk.eyJ1IjoiYWxhc3NldHRlciIsImEiOiI3andLQ1MwIn0.1q6yuZJaMCnAW0cm_6vy_A"

const navStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  padding: '10px'
}
const defaultConfig = {
  latitude: 32.739288,
  longitude: -97.567164,
  zoom: 8,
  bearing: 0,
  pitch: 0
}

export default function Map() {
  const mapRef = useRef()
  const [viewport, setViewport] = useState(defaultConfig)
  const [mapStyle, setMapStyle] = useState(defaultMapStyle)
  const [currentlySelected, setCurrentlySelected] = useState()
  const mapState = useCallback(state => ({ records: state.records, centerpoint: state.centerpoint }))
  const { records, centerpoint } = useMappedState(mapState)
  const dispatch = useDispatch()
  const populate = useCallback(records => dispatch(populateRecords(records)))
  const select = useCallback(record => dispatch(selectRecord(record)))
  const setTranslate = useCallback(coordinates => dispatch(setTranslateTo(coordinates)))
  const updateViewport = useCallback(viewport => setViewport(viewport))

  useEffect(() => {
    if (records) {
      const geojson = fromJS({ type: 'geojson', data: records })
      let updatedMapLayers = defaultMapStyle.get('layers')
      updatedMapLayers = updatedMapLayers.push(dataLayer)
      updatedMapLayers = updatedMapLayers.push(selectedLayer)
      const updatedMapStyle = defaultMapStyle
        .setIn(['sources', 'records'], geojson)
        .set('layers', updatedMapLayers)
      // const [latitude, longitude] = center(
      //   polygon(records.features[0].geometry.coordinates)
      // ).geometry.coordinates
      setViewport({ ...viewport, zoom: 14 })
      setMapStyle(updatedMapStyle)
    }
  }, [records])

  useEffect(() => {
    const map = mapRef.current.getMap()
    map.flyTo(centerpoint)
    setViewport({...viewport, zoom: 16})
  }, [centerpoint])

  useEffect(() => {
    const indexOfListToUpdate = mapStyle.get('layers').findIndex(listItem => listItem.get('id') === 'hover')
    let updatedMapStyle = mapStyle
    if (currentlySelected) {
      updatedMapStyle = updatedMapStyle.setIn(['layers', indexOfListToUpdate, 'filter', 2], currentlySelected.id)
    } else {
      const nullValue = updatedMapStyle.get('layers').get(indexOfListToUpdate).get('filter').get(2)
      updatedMapStyle = updatedMapStyle.setIn(['layers', indexOfListToUpdate, 'filter', 2], nullValue)
    }
    setMapStyle(updatedMapStyle)
  }, [currentlySelected])

  if (!records) {
    setTimeout(() => {
      populate(GEOJSON)
    }, 1000)
  }

  function hoverHandler(event) {
    setCurrentlySelected(
      event.features && (event.features.filter(feature => feature.properties.id)[0] || {}).properties
    )
  }

  function clickHandler(event) {
    const feature = event.features && event.features.filter(feature => feature.properties.id)[0]
    if (!feature) {
      setTranslate(event.lngLat)
    } else {
      select(currentlySelected)
    }
  }

  return (
    <ReactMapGL
      {...viewport}
      width="100%"
      height="100%"
      // draggable={false}
      mapStyle={mapStyle}
      ref={mapRef}
      onViewportChange={updateViewport}
      onHover={hoverHandler}
      onClick={clickHandler}
      mapboxApiAccessToken={MAPBOX_TOKEN}>
      <div className="nav" style={navStyle}>
        <NavigationControl onViewportChange={updateViewport} />
      </div>
    </ReactMapGL>
  )
}