const path = require('path')
// const HtmlWebPackPlugin = require('html-webpack-plugin')
// const ManifestRevisionPlugin = require('manifest-revision-webpack-plugin')
const rootAssetPath = __dirname
module.exports = {
    entry: {
        app_js: rootAssetPath + '/scripts/index.js',
        app_css: rootAssetPath + '/styles/main.css',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: 'http://localhost:2992/assets/',
        filename: '[name].js',
        chunkFilename: '[id].[chunkhash].js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "css-loader"
                    }
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "@babel/preset-env",
                            "@babel/preset-react"
                        ]
                    }
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            }
        ]
    },
    plugins: [
        /*new HtmlWebPackPlugin({
            template: "./index.html",
            filename: "./index.html"
        }),*/
        /*new ManifestRevisionPlugin(path.join(__dirname, '..', 'shared', 'manifest.json'), {
            rootAssetPath: rootAssetPath,
            ignorePaths: ['/styles', '/scripts']
        })*/
    ]
}
